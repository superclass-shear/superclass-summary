import numpy as np
import sys
import glob
import pdb
import configparser

from astropy.table import Table
from astropy.io import fits

from matplotlib import pyplot as plt
from matplotlib import rc


rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

config = configparser.ConfigParser()
config.read(sys.argv[-1])

datafile_list = config.get('files', 'catalogue').split(',')
imagefile_list = config.get('files', 'image').split(',')

for i, datafile in enumerate(datafile_list):
  plt.close('all')
  imagefile = imagefile_list[i]
  
  print(imagefile)
  source_cat = Table.read(datafile, format='fits')
  source_header = fits.getheader(imagefile)

  #jvla
  #bmaj = 5.12895756401E-04
  #bpa = 8.01413116455E+01

  #emerlin
  bmaj = 1.1596E-04
  bmin = 6.4556E-05
  bpa = 147.93
  resolved_cut = (source_cat['DC_Min']>0)*(source_cat['DC_Maj']>0)*(source_cat['Maj']>bmaj)
  resolved_cat = source_cat[resolved_cut]

  int_peak_ratio = source_cat['Total_flux']/source_cat['Peak_flux']
  peak_snr = source_cat['Peak_flux']/source_cat['Resid_Isl_rms']
  size_res_critera = (source_cat['Maj']*source_cat['Min'])/(bmaj*bmin)

  plt.figure(1, figsize=(9, 7.5))
  plt.subplot(221)
  n, bins, patches = plt.hist(source_cat['Maj']*3600., color='powderblue', normed=False, bins=np.linspace(0.0, 5, 15))
  plt.hist((resolved_cat['Maj']*3600.), bins=bins, color='lightcoral', normed=False)
  plt.axvline(bmaj*3600., color='k', label='$\mathrm{Beam}$')
  plt.legend()
  plt.xlabel('$\mathrm{Major \, axis \,\, [arcsec]}$')

  plt.subplot(222)
  n, bins, patches = plt.hist(np.log10(source_cat['Total_flux']), color='powderblue', normed=False, bins=np.linspace(-5.0, -2.0, 15))
  plt.hist(np.log10(resolved_cat['Total_flux']),  bins=bins, color='lightcoral', label='$a_{\\rm gal} > a_{\\rm beam}$', normed=False)
  plt.xlabel('$\log_{10}(\mathrm{Flux \,\, [Jy]})$')

  plt.subplot(223)
  n, bins, patches = plt.hist(source_cat['PA'], color='powderblue', normed=False)
  plt.hist(resolved_cat['DC_PA'],  bins=bins, color='lightcoral', label='$a_{\\rm gal} > a_{\\rm beam}$', normed=False)
  plt.axvline(bpa, color='k')
  plt.xlabel('$\mathrm{Position \, Angle \,\, [deg]}$')

  plt.subplot(224)
  q = resolved_cat['DC_Min']/resolved_cat['DC_Maj']
  mod_e = (1. - q**2.)/(1. + q**2.)
  n, bins, patches = plt.hist(mod_e, color='powderblue', normed=False)
  plt.hist(mod_e,  bins=bins, color='lightcoral', label='$a_{\\rm gal} > a_{\\rm beam}$', normed=False)
  plt.xlabel('$\mathrm{Ellipticity} \,\, |e|$')
  plt.suptitle((datafile.split('/')[-1]).split('.')[0].replace('_','-'), y=0.925)
  plt.savefig(datafile+'_summary.png', bbox_inches='tight', dpi=300)

  plt.figure(2, figsize=(4.5, 3.75))
  plt.plot(peak_snr, int_peak_ratio, 'o', color='powderblue')
  plt.plot(peak_snr[resolved_cut], int_peak_ratio[resolved_cut], 'o', color='lightcoral')
  plt.xscale('log')
  plt.legend(['$\mathrm{All}$', '$\\mathrm{Maj_{DC} \, \& \, Min_{DC}} > 0$'], loc='upper right', fontsize='small')
  plt.xlabel('$S_{\\rm peak}/\sigma_{\\rm Resid\_Isl\_rms}$')
  plt.ylabel('$S_{\\rm int}/S_{\\rm peak}$')
  plt.axvline(5, color='k', linestyle='--')
  plt.axhline(1, color='k', linestyle='--')
  x = np.linspace(peak_snr.min(),peak_snr.max(), 10)
  y1 = np.ones_like(x)*(int_peak_ratio - 1).min()
  plt.fill_between(x, 1+y1, 1+abs(y1), alpha=0.3, color='k', linestyle='None', edgecolor='None', linewidth=0)
  plt.xlim([peak_snr.min(),peak_snr.max()])
  plt.savefig('flatn49_v1.6-masked-gt100ujy-resid_isl_rms.png', bbox_style='tight', dpi=300)

import bdsf
import numpy as np
import sys
import glob

from astropy.table import Table
from astropy.io import fits

if sys.argv[-1].split('.')[-1] == 'fits':
  imagefile_list = sys.argv[1:]
else:
  #imagefile_list = glob.glob(sys.argv[-1]+'*image.fits')
  imagefile_list = glob.glob(sys.argv[-1]+'*.fits')

for imagefile in imagefile_list:

  img = bdsf.process_image(imagefile, adaptive_rms_box = True,
                                      adaptive_thresh = 50,
                                      rms_box_bright = (50,10),
                                      rms_box = (200,10),
                                      catalog_type='srl')

  img.write_catalog(format='fits', clobber=True)
  img.write_catalog(format='ds9', clobber=True)

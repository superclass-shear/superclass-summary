import glob
import numpy as np
import os

data_dir = '/share/c10/harrison/jvla_dr1/'

tarballs = glob.glob(data_dir+'*.tgz')

for pointing_tarball in tarballs:
  
  os.chdir(data_dir)
  pointing = pointing_tarball.split('/')[-1].rstrip('.tgz')
  os.system('mkdir {0}'.format(data_dir+pointing))
  os.chdir(data_dir+pointing)
  os.system('mv {0} .'.format(pointing_tarball))
  os.system('tar -xzvf {1} jvla_dr1_{0}_I_B.residual.tt0/'.format(pointing, pointing_tarball.split('/')[-1]))
  
  exportfits(imagename=data_dir+'{0}/jvla_dr1_{0}_I_B.residual.tt0/'.format(pointing, pointing),
             fitsimage=data_dir+'{0}/jvla_dr1_{0}_I_B.residual.tt0.fits'.format(pointing, pointing))

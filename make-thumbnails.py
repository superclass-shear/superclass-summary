'''
usage: python make-thumbnails.py inifile
for an example ini file see inis/example-make-thumbnails.ini
'''

import numpy as np
import sys
import glob
import pdb
import configparser

from astropy import wcs
from astropy.table import Table
from astropy.io import fits
from astropy import units as u

from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

config = configparser.ConfigParser()
config.read(sys.argv[-1])

fov = config.getfloat('images', 'fov')*u.arcsec

catfile = config.get('files', 'catalogue')
imagefiles = config.get('files', 'images').split(',')
image_titles = config.get('files', 'image_titles').split(',')

n_images = len(imagefiles)

cat = Table.read(catfile, format='fits')
cat = cat[cat['Total_flux']>config.getfloat('images','flux_cut')]

for i,source in enumerate(cat):

  plt.close('all')
  fig = plt.figure(1, figsize=(n_images*4.5, 3.75))
  fig.subplots_adjust(wspace=0, hspace=0)
  
  for i_im,imagefile in enumerate(imagefiles):
    header = fits.getheader(imagefile)
    image = fits.getdata(imagefile)
    image_wcs = wcs.WCS(header)
    if (image_wcs.naxis == 4):
      image = image[0,0]
      image_wcs = image_wcs.dropaxis(2)
      image_wcs = image_wcs.dropaxis(2)

    y, x = image_wcs.wcs_world2pix(source['RA'], source['DEC'], 1)

    y = int(y)
    x = int(x)

    pix_scale = image_wcs.pixel_scale_matrix[1,1]*u.deg
    npix = int(round(fov.value / pix_scale.to(u.arcsec).value))

    thumb = image[x-npix:x+npix,y-npix:y+npix]

    if image_wcs.wcs.cdelt[0] < 0:
      thumb = thumb[:,::-1]
    
    #plt.title(('$ %.2e' % (source['Total_flux']*1.e6) )+'\, \mu\mathrm{Jy}$')
    #snr = source['Total_flux']/source['Resid_Isl_rms']
    #plt.title('$\mathrm{SNR} \, '+('%.1f$' % snr ))

    if (i_im == 0):
      ax = fig.add_subplot(1,n_images,i_im+1, projection=image_wcs[x-npix:x+npix,y-npix:y+npix])
      plt.imshow(thumb, cmap='inferno', origin='lower', interpolation='nearest')
      source_coord = wcs.utils.pixel_to_skycoord(y,x, image_wcs)
      source_name_ra = str(int(source_coord.ra.hms.h))+str(int(source_coord.ra.hms.m))+('%.2f' % source_coord.ra.hms.s)
      source_name_dec = str(int(source_coord.dec.dms.d))+str(int(source_coord.dec.dms.m))+('%.2f' % source_coord.dec.dms.s)
      source_name_pos = 'J'+source_name_ra+'+'+source_name_dec
      plt.xlabel('$\mathrm{RA}$')
      plt.ylabel('$\mathrm{Dec}$')
      plt.title(image_titles[i_im])
      plt.suptitle(config.get('images', 'source_prefix')+source_name_pos)
    else:
      ax = fig.add_subplot(1,n_images,i_im+1, projection=image_wcs[x-npix:x+npix,y-npix:y+npix], xticklabels=[], yticklabels=[])
      plt.imshow(thumb, cmap='inferno', origin='lower', interpolation='nearest')
      plt.title(image_titles[i_im])

    try:
      bmaj_pix = header['BMAJ']*u.deg / np.abs(header['CDELT1']*u.deg)
      bmin_pix = header['BMIN']*u.deg / np.abs(header['CDELT1']*u.deg)
      bpa = header['BPA']*u.deg
      psf_ellipse = Ellipse(xy=[2*npix-0.2*npix,0.2*npix], width=bmaj_pix.value, height=bmin_pix.value, angle=bpa.value, edgecolor='white', facecolor='none', linewidth=1)
      ax.add_artist(psf_ellipse)
    except:
      print('No PSF information available for {0}'.format(image_titles[i_im]))

  fig.subplots_adjust(wspace=0, hspace=0)
  outdir = config.get('files', 'output_directory')
  outprefix = config.get('images', 'source_prefix')
  plt.savefig(outdir+'/'+outprefix+'-'+source_name_pos+'.png', bbox_inches='tight', dpi=300)
  print(i, len(cat), source_name_pos)
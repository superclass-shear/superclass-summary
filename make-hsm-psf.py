import numpy as np
import sys
import glob
import pdb

from astropy.table import Table
from astropy.io import fits

from matplotlib import pyplot as plt
from matplotlib import rc

from astropy.io import fits

import galsim

psf_fname = sys.argv[-1]
clean_fname = psf_fname.replace('psf', 'image')

npix=47

dirty_psf = fits.getdata(psf_fname)
dirty_psf = dirty_psf[0,0]
psf_header = fits.getheader(psf_fname)
clean_psf_header = fits.getheader(clean_fname)

pix_scale = np.abs(psf_header['CDELT1'])*galsim.degrees
pix_scale = pix_scale / galsim.arcsec

extent = np.linspace(-npix*pix_scale, npix*pix_scale, 2*npix)

dirty_psf_image = galsim.Image(dirty_psf, scale=pix_scale)
clean_psf_image = galsim.Image(np.zeros_like(dirty_psf), scale=pix_scale)
moms = galsim.hsm.FindAdaptiveMom(dirty_psf_image)

clean_psf_q = clean_psf_header['BMIN']/clean_psf_header['BMAJ']
clean_psf_pa = clean_psf_header['BPA']*galsim.degrees - 90*galsim.degrees
clean_psf_fwhm = clean_psf_header['BMAJ']*galsim.degrees 

clean_psf = galsim.Gaussian(fwhm=clean_psf_fwhm/galsim.arcsec)
clean_psf = clean_psf.shear(q=clean_psf_q, beta=clean_psf_pa)

hsm_psf_sigma = moms.moments_sigma
hsm_psf_e1 = moms.observed_shape.e1
hsm_psf_e2 = moms.observed_shape.e2
hsm_psf_e = np.sqrt(hsm_psf_e1**2. + hsm_psf_e2**2.)
hsm_psf_q = (1. - hsm_psf_e)/(1. + hsm_psf_e)
hsm_psf_pa = 0.5*np.arctan2(hsm_psf_e2, hsm_psf_e1)*galsim.radians

hsm_psf = galsim.Gaussian(moms.moments_sigma)
hsm_psf = hsm_psf.shear(moms.observed_shape)
hsm_psf_fwhm = pix_scale*hsm_psf.calculateFWHM()*galsim.arcsec
hsm_psf = galsim.Gaussian(fwhm=hsm_psf_fwhm/galsim.arcsec)
hsm_psf = hsm_psf.shear(moms.observed_shape)

hsm_psf_image = galsim.Image(npix*2, npix*2, scale=pix_scale)
hsm_psf_stamp = hsm_psf.drawImage(image=hsm_psf_image)
psf_guassian_stamp_image = hsm_psf_stamp
hsm_psf_stamp_array = hsm_psf_stamp.array
hsm_psf_stamp_array = hsm_psf_stamp_array/hsm_psf_stamp_array.max()

clean_psf_image = galsim.Image(npix*2, npix*2, scale=pix_scale)
clean_psf_stamp = clean_psf.drawImage(image=clean_psf_image)
clean_psf_stamp_array = clean_psf_stamp.array
clean_psf_stamp_array = clean_psf_stamp_array/clean_psf_stamp_array.max()

plt.close('all')
plt.figure(1, figsize=(13.5,9))
plt.subplot(231)
plt.plot(extent, dirty_psf[dirty_psf.shape[0]/2,dirty_psf.shape[0]/2 - npix:dirty_psf.shape[0]/2 + npix], color='powderblue', label='$\mathrm{Dirty \, Beam}$')
plt.plot(extent, hsm_psf_stamp_array[hsm_psf_stamp_array.shape[0]/2], color='lightcoral', label='$\mathrm{GalSim \, HSM \, Fit}$')
plt.plot(extent, clean_psf_stamp_array[clean_psf_stamp_array.shape[0]/2], color='palegreen', label='$\mathrm{WSCLEAN \, Fit}$')
plt.legend(loc='upper left', fontsize='x-small')
plt.xlim([extent[0], extent[-1]])
plt.xlabel('$\mathrm{RA \, [arcsec]}$')
plt.subplot(232)
plt.plot(extent, dirty_psf[dirty_psf.shape[0]/2 - npix:dirty_psf.shape[0]/2 + npix, dirty_psf.shape[0]/2], color='powderblue')
plt.plot(extent, hsm_psf_stamp_array[:,hsm_psf_stamp_array.shape[0]/2], color='lightcoral')
plt.plot(extent, clean_psf_stamp_array[:,clean_psf_stamp_array.shape[0]/2], color='palegreen')
plt.xlim([extent[0], extent[-1]])
plt.xlabel('$\mathrm{Dec \, [arcsec]}$')
plt.subplot(234)
x0, y0 = dirty_psf.shape[0]/2, dirty_psf.shape[1]/2
xlow, ylow = x0 - npix, y0 - npix
xup, yup = x0 + npix, y0+npix
plt.imshow((dirty_psf[xlow:xup, ylow:yup]), cmap='gnuplot2')
plt.axis('off')
plt.title('$\mathrm{Dirty}$')
plt.subplot(235)
plt.imshow((clean_psf_stamp_array), cmap='gnuplot2')
plt.text(5, 7.5 , '{0}arcsec\n{1} arcsec\n{2} deg'.format(clean_psf_fwhm/galsim.arcsec, clean_psf_q*clean_psf_fwhm/galsim.arcsec, clean_psf_pa/galsim.degrees), color='white')
plt.axis('off')
plt.title('$\mathrm{WSCLEAN \, Fit}$')
plt.subplot(236)
plt.imshow((hsm_psf_stamp_array), cmap='gnuplot2')
plt.text(5, 7.5 , '{0}arcsec\n{1} arcsec\n{2} deg'.format(hsm_psf_fwhm/galsim.arcsec, hsm_psf_q*hsm_psf_fwhm/galsim.arcsec, hsm_psf_pa/galsim.degrees), color='white')
plt.axis('off')
plt.title('$\mathrm{HSM \, Fit}$')
plt.savefig('hsm-psf-'+ psf_fname.split('/')[-1]+'.png', bbox_inches='tight', dpi=300)

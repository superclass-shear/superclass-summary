import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc

from astropy.table import Table

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

'''
Matching fractions as a function of:
- jvla flux
- emerlin flux
- subaru flux

Absolute numbers:
- subaru-emerlin matches
- subaru-jvla matches
- resolved emerlin-subaru matches
- resolved emerlin-jvla-subaru matches
'''

data_dir = '/Users/harrison/Dropbox/superclass-data-repository/catalogues/'

emerlin_cat = Table.read(data_dir+'emerlin/FLATN49-image.pybdsm.gaul.fits')
jvla_7jy_cat = Table.read(data_dir+'jvla/jvla_dr1_quicklook_L190u-D24u_mixedres.mlinmos.pybdsm.gaul.fits')
jvla_11jy_cat = Table.read(data_dir+'jvla/jvla_dr1_quicklook_c40u_mixedres.mlinmos.pybdsm.gaul.fits')
subaru_cat = Table.read(data_dir+'subaru/sclass_iband_shape_cat.sort.fits')

emerlin_jvla_7jy_cat = Table.read(data_dir+'emerlin-jvla-matches.fits')
emerlin_jvla_11jy_cat = Table.read(data_dir+'emerlin-jvla_11ujy-matches.fits')
emerlin_subaru_cat = Table.read(data_dir+'emerlin-subaru-matches.fits')
jvla_7jy_subaru_cat = Table.read(data_dir+'jvla-subaru-matches.fits')
emerlin_jvla_7jy_subaru_cat = Table.read(data_dir+'emerlin-jvla-subaru-matches.fits')

emerlin_counts, emerlin_bins, _ = plt.hist(np.log10(emerlin_cat['Total_flux']), bins=25, range=(-5,-2))
emerlin_subaru_counts, emerlin_subaru_bins, _ = plt.hist(np.log10(emerlin_subaru_cat['Total_flux']), bins=25, range=(-5,-2))

emerlin_bin_centres = (emerlin_bins[:-1] + emerlin_bins[1:])/2

jvla_7jy_counts, jvla_7jy_bins, _ = plt.hist(np.log10(jvla_7jy_cat['Total_flux']), bins=25, range=(-5,-2))
jvla_7jy_subaru_counts, jvla_7jy_subaru_bins, _ = plt.hist(np.log10(jvla_7jy_subaru_cat['Total_flux']), bins=25, range=(-5,-2))

jvla_7jy_bin_centres = (jvla_7jy_bins[:-1] + jvla_7jy_bins[1:])/2

jvla_11jy_counts, jvla_11jy_bins, _ = plt.hist(np.log10(jvla_11jy_cat['Total_flux']), bins=25, range=(-5,-2))
#jvla_11jy_subaru_counts, jvla_11jy_subaru_bins, _ = plt.hist(np.log10(jvla_11jy_subaru_cat['Total_flux']), bins=25, range=(-5,-2))

jvla_11jy_bin_centres = (jvla_11jy_bins[:-1] + jvla_11jy_bins[1:])/2

plt.close('all')
plt.figure(1, figsize=(4.5, 3.75))
plt.subplot(111)
plt.suptitle('$\mathrm{Radio}$-$\mathrm{Optical \, Matches}$')
#plt.title('$\mathrm{e}$-$\mathrm{MERLIN}$-$\mathrm{Subaru}$')
plt.bar(jvla_7jy_bin_centres, jvla_7jy_subaru_counts/jvla_7jy_counts, width=0.12, color='lightcoral', alpha=1.0, label='$\mathrm{JVLA} \, 7.5 \, \mu\mathrm{Jy}$')
plt.bar(emerlin_bin_centres, emerlin_subaru_counts/emerlin_counts, width=0.12, color='powderblue', alpha=0.5, label='$\mathrm{e}$-$\mathrm{MERLIN}$')
plt.yscale('log')
plt.ylim([5.e-2, 10.e-1])
plt.yticks((1.e-1, 1.e0), ('$10\%$', '$100\%$'))
plt.xlabel('$\log_{10}(\mathrm{Flux \, [Jy]})$')
plt.ylabel('$\mathrm{Matching \, Fraction \, (of \, radio \, sources)}$')
plt.legend()
plt.savefig('radio-optical-flux.png', bbox_inches='tight', dpi=300)

emerlin_counts, emerlin_bins, _ = plt.hist(np.log10(emerlin_cat['Total_flux']), bins=25, range=(-5,-2))
emerlin_jvla_7jy_counts, emerlin_jvla_7jy_bins, _ = plt.hist(np.log10(emerlin_jvla_7jy_cat['Total_flux_1']), bins=25, range=(-5,-2))
emerlin_jvla_11jy_counts, emerlin_jvla_11jy_bins, _ = plt.hist(np.log10(emerlin_jvla_11jy_cat['Total_flux_1']), bins=25, range=(-5,-2))

emerlin_bin_centres = (emerlin_bins[:-1] + emerlin_bins[1:])/2

plt.figure(2, figsize=(4.5, 3.75))
plt.subplot(111)
#plt.title('$\mathrm{e}$-$\mathrm{MERLIN}$-$\mathrm{Subaru}$')
plt.ylabel('$\mathrm{Matching \, Fraction \, (of \, e}$-$\mathrm{MERLIN \, sources)}$')
plt.bar(emerlin_bin_centres, emerlin_jvla_7jy_counts/emerlin_counts, width=0.12, color='lightcoral', alpha=1.0, label='$\mathrm{JVLA} \, 7.5 \, \mu\mathrm{Jy}$')
plt.bar(emerlin_bin_centres, emerlin_jvla_11jy_counts/emerlin_counts, width=0.12, color='powderblue', alpha=0.5, label='$\mathrm{JVLA} \, 11 \, \mu\mathrm{Jy}$')
plt.xlabel('$\log_{10}(\mathrm{Flux_{eMERLIN} \, [Jy]})$')
plt.legend()
plt.savefig('emerlin-jvla-flux.png', bbox_inches='tight', dpi=300)

plt.figure(3, figsize=(4.5, 3.75))
plt.subplot(111)
#plt.title('$\mathrm{e}$-$\mathrm{MERLIN}$-$\mathrm{Subaru}$')
plt.ylabel('$\mathrm{Matching \, Fraction \, (of \, JVLA \, 7 \, \mu Jy \, sources)}$')
plt.bar(jvla_7jy_bin_centres, emerlin_jvla_7jy_counts/emerlin_jvla_11jy_counts, width=0.12, color='lightcoral', alpha=1.0)
plt.xlabel('$\log_{10}(\mathrm{Flux}_{\mathrm{JVLA} 7.5 \,\mu} \, \mathrm{[Jy]})$')
plt.legend()
plt.savefig('jvla-jvla-flux.png', bbox_inches='tight', dpi=300)

print('e-MERLIN: ', len(emerlin_cat))
print('JVLA: ', len(jvla_7jy_cat))
print('Subaru: ', len(subaru_cat))
print('e-MERLIN-JVLA matches: ', len(emerlin_jvla_7jy_cat))
print('e-MERLIN-Subaru matches: ', len(emerlin_subaru_cat))
print('JVLA-Subaru matches: ', len(jvla_7jy_subaru_cat))
print('e-MERLIN-Subaru matches resolved in both: ', len(emerlin_subaru_cat[(emerlin_subaru_cat['Maj']>1.1596E-04)]))
print('JVLA-Subaru matches resolved in both: ', len(jvla_7jy_subaru_cat[(jvla_7jy_subaru_cat['Maj']>5.12895756401E-04)]))
print('e-MERLIN-JVLA-Subaru matches resolved in all three: ', len(emerlin_jvla_7jy_subaru_cat[(emerlin_jvla_7jy_subaru_cat['Maj_1']>1.1596E-04)*(emerlin_jvla_7jy_subaru_cat['Maj_2']>5.12895756401E-04)]))

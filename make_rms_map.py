import numpy as np
import numpy.fft as fft
from scipy.ndimage import fourier_gaussian
from scipy.misc import imresize
from astropy import wcs
import pdb
import astropy.io.fits as fits
import pylab as plt
import sys
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all')

def rebin(a, shape):
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).sum(-1).sum(1)

ffile = sys.argv[1]
hdu = fits.open(ffile)
data = hdu[0].data
header = hdu[0].header
image_wcs = wcs.WCS(header)

if (image_wcs.naxis == 4):
  image_wcs = image_wcs.dropaxis(2)
  image_wcs = image_wcs.dropaxis(2)

if len(data.shape)==4:
  data = data[0,0,:,:]

#print data
data_shape_org = data.shape

#data.shape = (70000,70000)

npix = data.shape

sdat  = rebin(data,np.asarray(data.shape)/10)
sdat2 = rebin(data*data,np.asarray(data.shape)/10)
ndat = 10*10

rms = np.sqrt((sdat2-sdat*sdat/ndat)/ndat)*1e6
rms = np.where(rms!=rms,40,rms)

sig = 200

ftrms = fft.fft2(rms)
ftg = fourier_gaussian(ftrms,sigma=sig)
smo = fft.ifft2(ftg)
smo = smo.real
smo = smo.astype(np.float)

<<<<<<< HEAD
plt.imshow(rms,vmin=0,vmax=25,cmap='gnuplot2')
=======
fig = plt.figure(1, figsize=(9, 7.5))
ax = fig.add_subplot(111, projection=image_wcs)
plt.imshow(rms,vmin=0,vmax=25,cmap='magma',origin='lower',interpolation='nearest')
>>>>>>> 842394fd62805c7d4cd0af6a02d2f4da16d65a47

lev = np.array([7.0, 7.5, 8.0, 9.0, 10.0])
cbar = plt.colorbar(orientation='vertical')
cbar.set_label('$\sigma_{\\rm RMS} \, [\mu \mathrm{Jy}]$')
plt.xlabel('$\mathrm{RA \, [deg]}$')
plt.ylabel('$\mathrm{DEC \, [deg]}$')

#plt.imshow(smo,cmap='magma')
<<<<<<< HEAD
cs = plt.contour(smo,lev)
#plt.clabel(cs,inline=1,fontsize=10)
=======
#cs = plt.contour(smo,lev, colors='white')
cs = plt.contour(smo, colors='white')
plt.clabel(cs,colors='white',inline=1,fontsize=10)
>>>>>>> 842394fd62805c7d4cd0af6a02d2f4da16d65a47

plt.title(ffile.split('/')[-1].replace('_', '\_')) 

plt.savefig(ffile+'_rms10x10.png', bbox_inches='tight', dpi=300)

hdu2 = fits.PrimaryHDU(rms)
<<<<<<< HEAD
hdu2.writeto('Flatn49_2.0_rms10x10.fits')
=======
hdu2.writeto(ffile+'_rms10x10.fits', overwrite=True)
>>>>>>> 842394fd62805c7d4cd0af6a02d2f4da16d65a47

#smo2 = imresize(smo, (70000,70000))
#smo2 = np.where(smo2>7.5, 0, 1)
#smo2.shape = data_shape_org
#smo2.astype(np.float)

#hdu[0].data[:] = smo2
#hdu.writeto('mask_v2.0_7.5uJy_rms.fits')
